package spring.boot.docker.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.boot.docker.demo.model.Users;
import spring.boot.docker.demo.model.UsersRepository;

import java.util.List;

@RestController
@RequestMapping("/all")
public class UserController {
    private UsersRepository usersRepository;

    public UserController(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @GetMapping("/")
    public List<Users> all()
    {
        return usersRepository.findAll();
    }

    @GetMapping("/create")
    public List<Users>create()
    {
        Users user = new Users();
        user.setId(1);
        user.setName("Sam");
        user.setSalary(3400);
        user.setTeamName("Developer");

        usersRepository.save(user);
        return  usersRepository.findAll();
    }
}
